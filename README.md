# Sciter-pdfium

Sciter binding for [pdfium](https://pdfium.googlesource.com/pdfium/), it can be used to create/view/ PDFs.

## Features
* View local/url/encrypted PDFs
* Find text
* Select Text
* Scale / Zoom
* Create new PDF, add text, rectangles, paths, images.

## Platform
* Windows - x32, x64, (arm64 *untested)
* Linux - x32, x64
* MacOS - (Move the dylib next to the binary.)

## Installation
1. Clone this repo and navigate to that directory using the command line.

```cmd
git clone https://gitlab.com/sciter-engine/sciter-pdfium.git
cd sciter-pdfium
```

2. Substitute the sciter_SOURCE_DIR value in the provided commands with the path to the Sciter SDK before proceeding with the execution.
 
3. Run the following commands:

```cmd
cmake -S ./ -B ./build -D sciter_SOURCE_DIR='PATH/TO/SCITER/SDK/'
cmake --build ./build --config Release 
```
This will build and output sciter-pdfium.(dll/so/dylib) alongside pdfium.(dll/so/dylib) in the build/[Release] folder. Place both libs besides uSciter/YourApp.

4. Try [`pdf-view.htm`](example/pdf-view.htm) / [`pdf-text.htm`](example/pdf-text.htm) / [`pdf-create.htm`](example/pdf-create.htm) from the [`example`](example/) folder.

## Screenshot
![screenshot](example/screenshot.png)
&nbsp;
&nbsp;
#### * Note
> sciter-pdfium uses the [precompiled pdfium](https://github.com/bblanchon/pdfium-binaries) binaries for faster hastlefree build.