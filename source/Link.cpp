#include "Link.h"
#include "Annot.h"
#include "Util.hpp"
#include "fpdf_doc.h"

namespace PDFium {

  int PageLink::countWebLinks() {
    return FPDFLink_CountWebLinks(m_link_page.get());
  }

  sciter::value PageLink::getURL(int link_index) {
    int link_length = FPDFLink_GetURL(m_link_page.get(), link_index, NULL, 0);
    std::vector<unsigned short> buffer(link_length);
    std::fill(buffer.begin(), buffer.end(), u'\0');
    link_length = FPDFLink_GetURL(m_link_page.get(), link_index, buffer.data(), link_length);
    auto url = reinterpret_cast<const WCHAR*>(buffer.data());

    return sciter::value::make_string(url, link_length);
  }

  int PageLink::countRects(int link_index) {
    return FPDFLink_CountRects(m_link_page.get(), link_index);
  }

  sciter::value PageLink::getRect(int link_index, int rect_index) {
    double left, top, right, bottom;
  
    bool res = FPDFLink_GetRect(m_link_page.get(),
      link_index,
      rect_index,
      &left,
      &top,
      &right,
      &bottom);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("right", right);
    val.set_item("bottom", bottom);
    val.set_item("top", top);
    return val;
  }

  sciter::value PageLink::getTextRange(int link_index) {
    int start_char_index, char_count;
    FPDFLink_GetTextRange(m_link_page.get(), link_index, &start_char_index, &char_count);

    sciter::value val;
    val.set_item("start_char_index", start_char_index);
    val.set_item("char_count", char_count);
    return val;
  }

  void PageLink::closeWebLinks() {
    m_link_page.reset();
  }

  sciter::value Link::getDest(sciter::value fpdf_document){
    auto document = fpdf_document.get_asset<Doc>();
    FPDF_DEST fs_dest = FPDFLink_GetDest(document->m_doc.get(), m_link);
    auto dest = new Dest(fs_dest);
    return sciter::value::wrap_asset(dest);
  }

  sciter::value Link::getAction() {
    FPDF_ACTION fs_action = FPDFLink_GetAction(m_link);
    auto action = new Action(fs_action);
    return sciter::value::wrap_asset(action);
  }

  sciter::value Link::getAnnot(sciter::value fpdf_page) {
    auto page = fpdf_page.get_asset<Page>();
    ScopedFPDFAnnotation scoped_annot = ScopedFPDFAnnotation(FPDFLink_GetAnnot(page->unwrap(), m_link));
    auto annot = new Annot(scoped_annot);
    return sciter::value::wrap_asset(annot);
  }

  sciter::value Link::getAnnotRect() {
    FS_RECTF rect;
    bool response = FPDFLink_GetAnnotRect(m_link, &rect);

    if (!response)
      return sciter::value::make_error("Error in function getAnnotRect");

    sciter::value val;
    val.set_item("left", rect.left);
    val.set_item("bottom", rect.bottom);
    val.set_item("right", rect.right);
    val.set_item("top", rect.top);
    return val;
  }

  int Link::countQuadPoints() {
    return FPDFLink_CountQuadPoints(m_link);
  }

  sciter::value Link::getQuadPoints(int quad_index) {
    FS_QUADPOINTSF quad_points;
    bool response = FPDFLink_GetQuadPoints(m_link, quad_index, &quad_points);

    if(!response)
      return sciter::value::make_error("Error in function getQuadPoints");

    sciter::value val;
    val.set_item("x1", quad_points.x1);
    val.set_item("y1", quad_points.y1);
    val.set_item("x2", quad_points.x2);
    val.set_item("y2", quad_points.y2);
    val.set_item("x3", quad_points.x3);
    val.set_item("y3", quad_points.y3);
    val.set_item("x4", quad_points.x4);
    val.set_item("y4", quad_points.y4);
    return val;
  }



}