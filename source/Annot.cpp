#include "Annot.h"
#include "Object.h"
#include "Link.h"

namespace PDFium {

  void Annot::closeAnnot() {
    m_annot.reset();
  }

  int Annot::getSubtype() {
    return FPDFAnnot_GetSubtype(m_annot.get());
  }

  bool Annot::isObjectSupportedSubtype(int subtype) {
    return FPDFAnnot_IsObjectSupportedSubtype(subtype);
  }

  sciter::value Annot::updateObject(sciter::value object) {
    auto page_object = object.get_asset<PageObject>();
    FPDFAnnot_UpdateObject(m_annot.get(), page_object->unwrap());
    return this->wrap();
  }

  int Annot::addInkStroke(sciter::value points_array) {
    std::vector<FS_POINTF> fs_points = {};
    fs_points.reserve(points_array.length());

    for (int i = 0; i < points_array.length(); i++) {
      auto item = points_array.get_item(i).get<sciter::value>();
      auto x = item.get_item(0).get<float>();
      auto y = item.get_item(1).get<float>();
      fs_points.push_back({ x, y });
    }
 
    return FPDFAnnot_AddInkStroke(m_annot.get(), fs_points.data(), fs_points.size());
  }

  bool Annot::removeInkList() {
    return FPDFAnnot_RemoveInkList(m_annot.get());
  }

  sciter::value Annot::appendObject(sciter::value object) {
    auto page_object = object.get_asset<PageObject>();
    bool response = FPDFAnnot_AppendObject(m_annot.get(), page_object->unwrap());
    return (response) ? this->wrap() : sciter::value::make_error("Error in function appendObject");
  }

  int Annot::getObjectCount() {
    return FPDFAnnot_GetObjectCount(m_annot.get());
  }

  sciter::value Annot::getObject(int index) {
    ScopedFPDFPageObject obj(FPDFAnnot_GetObject(m_annot.get(), index));
    auto page_object = new PageObject(obj);
    return sciter::value::wrap_asset(page_object);
  }

  bool Annot::removeObject(int index) {
    return FPDFAnnot_RemoveObject(m_annot.get(), index);
  }

  sciter::value Annot::setColor(
    INT type, 
    UINT R,
    UINT G,
    UINT B,
    UINT A
  ) 
  {
    bool response = FPDFAnnot_SetColor(m_annot.get(), (FPDFANNOT_COLORTYPE)type, R, G, B, A);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setColor");
  }

  sciter::value Annot::getColor(INT type) {
    UINT R, G, B, A;
    bool response = FPDFAnnot_GetColor(m_annot.get(), (FPDFANNOT_COLORTYPE)type, &R, &G, &B, &A);
    return sciter::value::color(A << 24 | B << 16 | G << 8 | R);
  }

  bool Annot::hasAttachmentPoints() {
    return FPDFAnnot_HasAttachmentPoints(m_annot.get());
  }

  sciter::value Annot::setAttachmentPoints(unsigned quad_index, sciter::value quad_points) {
    FS_QUADPOINTSF fs_quad_points{};
    fs_quad_points.x1 = quad_points.get_item("x1").get<float>();
    fs_quad_points.y1 = quad_points.get_item("y1").get<float>();
    fs_quad_points.x2 = quad_points.get_item("x2").get<float>();
    fs_quad_points.y2 = quad_points.get_item("y2").get<float>();
    fs_quad_points.x3 = quad_points.get_item("x3").get<float>();
    fs_quad_points.y3 = quad_points.get_item("y3").get<float>();
    fs_quad_points.x4 = quad_points.get_item("x4").get<float>();
    fs_quad_points.y4 = quad_points.get_item("y4").get<float>();

    bool response = FPDFAnnot_SetAttachmentPoints(m_annot.get(), quad_index, &fs_quad_points);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setAttachmentPoints");
  }

  sciter::value Annot::appendAttachmentPoints(sciter::value quad_points) {
    FS_QUADPOINTSF fs_quad_points{};

    fs_quad_points.x1 = quad_points.get_item("x1").get<float>();
    fs_quad_points.y1 = quad_points.get_item("y1").get<float>();
    fs_quad_points.x2 = quad_points.get_item("x2").get<float>();
    fs_quad_points.y2 = quad_points.get_item("y2").get<float>();
    fs_quad_points.x3 = quad_points.get_item("x3").get<float>();
    fs_quad_points.y3 = quad_points.get_item("y3").get<float>();
    fs_quad_points.x4 = quad_points.get_item("x4").get<float>();
    fs_quad_points.y4 = quad_points.get_item("y4").get<float>();

    bool response = FPDFAnnot_AppendAttachmentPoints(m_annot.get(), &fs_quad_points);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function appendAttachmentPoints");
  }

  sciter::value Annot::getAttachmentPoints(unsigned quad_index) {
    FS_QUADPOINTSF quad_points;
    bool response = FPDFAnnot_GetAttachmentPoints(m_annot.get(), quad_index, &quad_points);
 
    if (!response)
      return sciter::value::make_error("Error in function getAttachmentPoints");

    sciter::value val;
    val.set_item("x1", quad_points.x1);
    val.set_item("y1", quad_points.y1);
    val.set_item("x2", quad_points.x2);
    val.set_item("y2", quad_points.y2);
    val.set_item("x3", quad_points.x3);
    val.set_item("y3", quad_points.y3);
    val.set_item("x4", quad_points.x4);
    val.set_item("y4", quad_points.y4);
    return val;
  }

  sciter::value Annot::setRect(sciter::value rect) {
    FS_RECTF fs_rect{};
    fs_rect.left = rect.get_item("left").get<float>();
    fs_rect.top = rect.get_item("top").get<float>();
    fs_rect.right = rect.get_item("right").get<float>();
    fs_rect.bottom = rect.get_item("bottom").get<float>();

    bool response = FPDF_CALLCONV FPDFAnnot_SetRect(m_annot.get(), &fs_rect);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function setRect");
  }

  sciter::value Annot::getRect()
  {
    FS_RECTF fs_rect;
    bool response = FPDFAnnot_GetRect(m_annot.get(), &fs_rect);

    if (!response)
      return sciter::value::make_error("Error in function getRect");
    
    sciter::value val;
    val.set_item("left", fs_rect.left);
    val.set_item("bottom", fs_rect.bottom);
    val.set_item("right", fs_rect.right);
    val.set_item("top", fs_rect.top);
    return val;
  }

  sciter::value Annot::getVertices() {
    unsigned long size = FPDFAnnot_GetVertices(m_annot.get(), NULL, 0);

    std::vector<FS_POINTF> fs_buffer(size);
    size = FPDFAnnot_GetVertices(m_annot.get(), fs_buffer.data(), fs_buffer.size());

    std::vector<sciter::value> buffer(size);
    std::transform(fs_buffer.begin(), fs_buffer.end(), buffer.begin(), [](FS_POINTF v) {
      return std::vector<float>({v.x, v.y});
    });

    return buffer;
  }

  unsigned Annot::getInkListCount() {
    return FPDFAnnot_GetInkListCount(m_annot.get());
  }

  sciter::value Annot::getInkListPath(unsigned path_index) {
    unsigned long size = FPDFAnnot_GetInkListPath(m_annot.get(), path_index, NULL, NULL);
    std::vector<FS_POINTF> fs_buffer(size);
    size = FPDFAnnot_GetInkListPath(m_annot.get(), path_index, fs_buffer.data(), fs_buffer.size());

    std::vector<sciter::value> buffer(size);
    std::transform(fs_buffer.begin(), fs_buffer.end(), buffer.begin(), [](FS_POINTF v) {
      return std::vector<float>({ v.x, v.y });
      });

    return buffer;
  }

  sciter::value Annot::getLine() {
    FS_POINTF start, end;
    bool response = FPDFAnnot_GetLine(m_annot.get(), &start, &end);
    
    if(!response)
      return sciter::value::make_error("Error in function getLine");

    sciter::value val;
    val.set_item("startX", start.x);
    val.set_item("startY", start.y);
    val.set_item("endX",   end.x);
    val.set_item("endY",   end.y);
    return val;
  }

  sciter::value Annot::setBorder(float horizontal_radius, float vertical_radius, float border_width) {
    bool response = FPDFAnnot_SetBorder(m_annot.get(), horizontal_radius, vertical_radius, border_width);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setBorder");
  }

  sciter::value Annot::getBorder() {
    float horizontal_radius, vertical_radius, border_width;
    bool response = FPDFAnnot_GetBorder(m_annot.get(), &horizontal_radius, &vertical_radius, &border_width);

    if (!response)
      return sciter::value::make_error("Error in function getBorder");

    sciter::value val;
    val.set_item("horizontal_radius", horizontal_radius);
    val.set_item("vertical_radius", vertical_radius);
    val.set_item("border_width", border_width);
    return val;
  }

  bool Annot::hasKey(sciter::string key) {
    return FPDFAnnot_HasKey(m_annot.get(), aux::w2utf(key));
  }

  int Annot::getValueType(sciter::string key) {
    return FPDFAnnot_GetValueType(m_annot.get(), aux::w2utf(key));
  }

  sciter::value Annot::setStringValue(sciter::string key, sciter::string value) {
    FPDF_WIDESTRING value_str = reinterpret_cast<FPDF_WIDESTRING>(value.data());
    bool response = FPDFAnnot_SetStringValue(m_annot.get(), aux::w2utf(key), value_str);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setStringValue");
  }

  sciter::value Annot::getStringValue(sciter::string key)
  {
    auto key_utf = aux::w2utf(key);
    unsigned long size = FPDFAnnot_GetStringValue(m_annot.get(), key_utf, NULL, 0);
    std::vector<FPDF_WCHAR> buffer(size);
    size = FPDFAnnot_GetStringValue(m_annot.get(), key_utf, buffer.data(), buffer.size());
    sciter::string value(buffer.begin(), buffer.end());
    return (size) ? value : sciter::value::make_error("Error in function getStringValue");
  }

  sciter::value Annot::getNumberValue(sciter::string key) {
    auto key_utf = aux::w2utf(key);
    float value;
    bool response = FPDFAnnot_GetNumberValue(m_annot.get(), key_utf, &value);
    return (response) ? value : sciter::value::make_error("Error in function getNumberValue");
  }

  sciter::value Annot::setAP(int appearanceMode, sciter::string value) {
    FPDF_WIDESTRING value_str = reinterpret_cast<FPDF_WIDESTRING>(value.data());
    bool response = FPDFAnnot_SetAP(m_annot.get(), appearanceMode, value_str);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setAP");
  }
  sciter::value Annot::getAP(int appearanceMode)
  {
    unsigned long size = FPDFAnnot_GetAP(m_annot.get(), appearanceMode, NULL, 0);
    std::vector<FPDF_WCHAR> buffer;
    size = FPDFAnnot_GetAP(m_annot.get(), appearanceMode, buffer.data(), buffer.size());
    sciter::string value(buffer.begin(), buffer.end());
    return (size) ? value : sciter::value::make_error("Error in function getAP");
  }

  sciter::value Annot::getLinkedAnnot(sciter::string key) {
    ScopedFPDFAnnotation scoped_annot( FPDFAnnot_GetLinkedAnnot(m_annot.get(), aux::w2utf(key)) );
    auto annot = new Annot(scoped_annot);
    return sciter::value::wrap_asset(annot);
  }

  int Annot::getFlags() {
    return FPDFAnnot_GetFlags(m_annot.get());
  }

  sciter::value Annot::setFlags(int flags) {
    bool response = FPDFAnnot_SetFlags(m_annot.get(), flags);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setFlags");
  }

  sciter::value Annot::getLink() {
    FPDF_LINK fs_link = FPDFAnnot_GetLink(m_annot.get());
    auto link = new Link(fs_link);
    return sciter::value::wrap_asset(link);
  }

}
