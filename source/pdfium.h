#ifndef __pdfium_h__
#define __pdfium_h__

#include "sciter-x.h"
#include "fpdfview.h"
#include "Doc.h"

namespace PDFium {

  sciter::value getError(unsigned long code){
    switch (code) {
      case FPDF_ERR_SUCCESS:
        return sciter::value::make_error("Success");
      case FPDF_ERR_UNKNOWN:
        return sciter::value::make_error("Unknown error");
      case FPDF_ERR_FILE:
        return sciter::value::make_error("File not found or could not be opened");
      case FPDF_ERR_FORMAT:
        return sciter::value::make_error("File not in PDF format or corrupted");
      case FPDF_ERR_PASSWORD:
        return sciter::value::make_error("Password Error");
      case FPDF_ERR_SECURITY:
        return sciter::value::make_error("Unsupported security scheme");
      case FPDF_ERR_PAGE:
        return sciter::value::make_error("Page not found or content error");
      default:
        return sciter::value::make_error("Unknown error");
    }
  }
  
  class PDF : public sciter::om::asset<PDF> {

    sciter::value ERR_SUCCESS = FPDF_ERR_SUCCESS;
    sciter::value ERR_UNKNOWN = FPDF_ERR_UNKNOWN;
    sciter::value ERR_FILE = FPDF_ERR_FILE;
    sciter::value ERR_FORMAT = FPDF_ERR_FORMAT;
    sciter::value ERR_PASSWORD = FPDF_ERR_PASSWORD;
    sciter::value ERR_SECURITY = FPDF_ERR_SECURITY;
    sciter::value ERR_PAGE = FPDF_ERR_PAGE;
  
    public:
      sciter::value loadDocument(sciter::astring file_path, sciter::astring password);
      sciter::value loadMemDocument(const sciter::value& buffer, sciter::astring password);
      sciter::value createNewDocument();
  
    PDF() {
      FPDF_LIBRARY_CONFIG config = { 0 };
      config.version = 3;
      config.m_pUserFontPaths = NULL;
      config.m_pIsolate = NULL;
      config.m_v8EmbedderSlot = 0;
      config.m_pPlatform = NULL;
      config.m_pUserFontPaths = NULL;
      FPDF_InitLibraryWithConfig(&config);
    };
  
    virtual ~PDF() {
      FPDF_DestroyLibrary();
    };
    
    SOM_PASSPORT_BEGIN(PDF)
    SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
    SOM_FUNCS(
      SOM_FUNC(loadDocument),
      SOM_FUNC(loadMemDocument),
      SOM_FUNC(createNewDocument)
    )
    SOM_PROPS(
      SOM_RO_PROP(ERR_SUCCESS),
      SOM_RO_PROP(ERR_UNKNOWN),
      SOM_RO_PROP(ERR_FILE),
      SOM_RO_PROP(ERR_FORMAT),
      SOM_RO_PROP(ERR_PASSWORD),
      SOM_RO_PROP(ERR_SECURITY),
      SOM_RO_PROP(ERR_PAGE)
    )
    SOM_PASSPORT_END
  };

}
#endif