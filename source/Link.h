#ifndef __link_h__
#define __link_h__

#include "Text.h"

namespace PDFium {

  struct Link : public sciter::om::asset<Link> {
    FPDF_LINK m_link = NULL;

    Link() : m_link(NULL) {}
    Link(FPDF_LINK link) : m_link(link) {}
    virtual ~Link() {}

    sciter::value getDest(sciter::value fpdf_document);
    sciter::value getAction();
    sciter::value getAnnot(sciter::value page);
    sciter::value getAnnotRect();
    int countQuadPoints();
    sciter::value getQuadPoints(int quad_index);

      SOM_PASSPORT_BEGIN(Link)
      SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
       SOM_FUNCS(
         SOM_FUNC(getDest),
         SOM_FUNC(getAction),
         SOM_FUNC(getAnnot),
         SOM_FUNC(getAnnotRect),
         SOM_FUNC(countQuadPoints),
         SOM_FUNC(getQuadPoints),
       )
    SOM_PASSPORT_END
  };

  struct PageLink : Text {
    
    ScopedFPDFPageLink m_link_page = {};
    PageLink(FPDF_PAGELINK& link) : m_link_page(link){}
    virtual ~PageLink() {
      closeWebLinks();
    }

    FPDF_PAGELINK unwrap() {
      return m_link_page.get();
    }

    int countWebLinks();
    sciter::value getURL(int link_index);
    int countRects(int link_index);
    sciter::value getRect(int link_index, int rect_index);
    sciter::value getTextRange(int link_index);
    void closeWebLinks();

    SOM_PASSPORT_BEGIN(PageLink)
    SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(countWebLinks),
        SOM_FUNC(getURL),
        SOM_FUNC(countRects),
        SOM_FUNC(getRect),
        SOM_FUNC(getTextRange),
        SOM_FUNC(closeWebLinks),
      )
    SOM_PASSPORT_END
  };
}

#endif