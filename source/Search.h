#ifndef __search_h__
#define __search_h__

#include "Text.h"

namespace PDFium {

  struct Search : Text {
    ScopedFPDFTextFind m_handle = {};
    sciter::value IGNORECASE = 0;
    sciter::value MATCHCASE = FPDF_MATCHCASE;
    sciter::value MATCHWHOLEWORD = FPDF_MATCHWHOLEWORD;
    sciter::value CONSECUTIVE = FPDF_CONSECUTIVE;

    Search(FPDF_SCHHANDLE& handle) : m_handle(handle){}
    virtual ~Search() {
      findClose();
    }

    FPDF_SCHHANDLE unwrap() {
      return m_handle.get();
    }

    bool findNext();
    bool findPrev();
    int getSchResultIndex();
    int getSchCount();
    void findClose();

    SOM_PASSPORT_BEGIN(Search)
    SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(findNext),
        SOM_FUNC(findPrev),
        SOM_FUNC(getSchResultIndex),
        SOM_FUNC(getSchCount),
        SOM_FUNC(findClose),
      )
      SOM_PROPS(
        SOM_RO_PROP(IGNORECASE),
        SOM_RO_PROP(MATCHCASE),
        SOM_RO_PROP(MATCHWHOLEWORD),
        SOM_RO_PROP(CONSECUTIVE),
      )

    SOM_PASSPORT_END
  };
}

#endif