#ifndef __font_hpp__
#define __font_hpp__

#include "Doc.h"

namespace PDFium {

  struct Font : Doc {

    ScopedFPDFFont m_font = {};
    int FONT_TYPE1 = FPDF_FONT_TYPE1;
    int FONT_TRUETYPE = FPDF_FONT_TRUETYPE;

    Font(ScopedFPDFDocument& doc, sciter::astring font_name) {
      m_font = ScopedFPDFFont(FPDFText_LoadStandardFont(doc.get(), font_name.c_str()));
    }

    Font(ScopedFPDFDocument& doc, const sciter::value& buffer, int font_type, bool cid = true) {
      auto cache = buffer.get<std::vector<uint8_t>>();
      m_font = ScopedFPDFFont( FPDFText_LoadFont(
        doc.get(), 
        &cache[0],
        cache.size(),
        font_type, 
        cid
      ));
    }

    FPDF_FONT unwrap() {
      asset_add_ref();
      return m_font.get();
    }

    virtual ~Font() {
      close();
      asset_release();
    }

    void close() {
      m_font.reset();
    }
    
    SOM_PASSPORT_BEGIN(Font)
      SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(close),
      )
      SOM_PROPS(
        SOM_RO_PROP(FONT_TYPE1),
        SOM_RO_PROP(FONT_TRUETYPE),
      )
    SOM_PASSPORT_END

  };
  
}

#endif