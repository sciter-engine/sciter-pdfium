#include "Doc.h"
#include "Object.h"
#include "Font.hpp"
#include "sciter-x-graphics.h"

namespace PDFium {
  
  int getBlockForJpeg(void* param, unsigned long pos, unsigned char* buf, unsigned long size) {
    std::vector<uint8_t>* data_vector = static_cast<std::vector<uint8_t>*>(param);
    if (pos + size < pos || size_t(pos + size) > data_vector->size())
      return 0;
    memcpy(buf, data_vector->data() + pos, size);
    return 1;
  }

  int Doc::getFileVersion() {
    int version;
    FPDF_GetFileVersion(m_doc.get(), &version);
    return version;
  }

  sciter::value Doc::addPage(int page_number, double width, double height) {
    try {
      auto page = new Page(m_doc, page_number - 1, width, height);
      return sciter::value::wrap_asset(page);
    }
    catch (sciter::value e) {
      return e;
    }
  }

  sciter::value Doc::loadPage(int page_number) {
    try {
      auto page = new Page(m_doc, page_number - 1);
      return sciter::value::wrap_asset(page);
    }
    catch (sciter::value e) {
      return e;
    }
  }

  int Doc::getPageCount() {
    return FPDF_GetPageCount(m_doc.get());
  }

  sciter::value Doc::renderPage(int page_number, double scale, int flags) {
    FPDF_PAGE page = FPDF_LoadPage(m_doc.get(), page_number - 1);
    int width = static_cast<int>(FPDF_GetPageWidth(page) * scale);
    int height = static_cast<int>(FPDF_GetPageHeight(page) * scale);
    int alpha = FPDFPage_HasTransparency(page) ? 1 : 0;

    ScopedFPDFBitmap bitmap(FPDFBitmap_Create(width, height, alpha));  // BGRx

    if (bitmap.get()) {
      int rotation = 0;
      //int flags = FPDF_ANNOT | FPDF_LCD_TEXT;
      FPDF_DWORD fill_color = alpha ? 0x00000000 : 0xFFFFFFFF;

      FPDFBitmap_FillRect(bitmap.get(), 0, 0, width, height, fill_color);
      FPDF_RenderPageBitmap(bitmap.get(), page, 0, 0, width, height, rotation, flags);
      BYTE* buffer = (BYTE*)FPDFBitmap_GetBuffer(bitmap.get());
      sciter::image img = sciter::image::create(width, height, (bool)alpha, buffer);

      FPDF_ClosePage(page);
      return img.to_value();
    }
    throw sciter::om::exception("Could not render page.");
  }

  void Doc::deletePage(int page_number) {
    FPDFPage_Delete(m_doc.get(), page_number - 1);
  }

  sciter::value Doc::createNewRect(float x, float y, float w, float h) {
    ScopedFPDFPageObject obj(FPDFPageObj_CreateNewRect(x, y, w, h));
    auto page_object = new PageObject(obj);
    return sciter::value::wrap_asset(page_object);
  }

  sciter::value Doc::newImageObject() {
    ScopedFPDFPageObject obj(FPDFPageObj_NewImageObj(m_doc.get()));
    auto page_object = new PageObject(obj);
    return sciter::value::wrap_asset(page_object);
  }

  bool Doc::loadJpegFile(const sciter::value& page, int count, sciter::value page_object, const sciter::value& buffer) {
    FPDF_FILEACCESS file_access = {};
    FPDF_PAGEOBJECT image_object = page_object.get_asset<PageObject>()->unwrap();
    std::vector<uint8_t> jpeg_bytes = buffer.get<std::vector<uint8_t>>();

    //FPDF_PAGE pages = page.get_asset<Page>()->unwrap();
    file_access.m_FileLen = static_cast<unsigned long>(jpeg_bytes.size());
    file_access.m_GetBlock = &getBlockForJpeg;
    file_access.m_Param = &jpeg_bytes;

    bool res = FPDFImageObj_LoadJpegFileInline(nullptr, 0, image_object, &file_access);
    return res;
  }

  bool Doc::setBitmap(const sciter::value& page, int count, sciter::value page_object, const sciter::value& buffer) {
    UINT w = 0, h = 0;
    sciter::bytes_writer image_bytes = {};
    //FPDF_PAGE pages = page.get_asset<Page>()->unwrap();
    FPDF_PAGEOBJECT image_object = page_object.get_asset<PageObject>()->unwrap();

    sciter::image image_bitmap = sciter::image::load(buffer.get_bytes());
    image_bitmap.dimensions(w, h);
    image_bitmap.save(image_bytes, SCITER_IMAGE_ENCODING_RAW, 100); //converting png/gif/webp to BGRA bitmap
    ScopedFPDFBitmap bitmap(FPDFBitmap_CreateEx(w, h, FPDFBitmap_BGRA, (void*)image_bytes.bb.data(), w * 4));
    return FPDFImageObj_SetBitmap(nullptr, 0, image_object, bitmap.get());
  }
  
  void Doc::saveAsCopy(sciter::astring file_path) {
    filestream.open(file_path.c_str(), std::ios_base::binary);
    FPDF_FILEWRITE::version = 1;
    FPDF_FILEWRITE::WriteBlock = writeBlockCallback;
    FPDF_SaveAsCopy(m_doc.get(), this, 0);
    filestream.close();
  }

  sciter::value Doc::createNewPath(float x, float y) {
    ScopedFPDFPageObject obj(FPDFPageObj_CreateNewPath(x, y));
    auto page_object = new PageObject(obj);
    return sciter::value::wrap_asset(page_object);
  }

  sciter::value Doc::newTextObject(sciter::astring font, float font_size) {
    ScopedFPDFPageObject obj(FPDFPageObj_NewTextObj(m_doc.get(), font.c_str(), font_size));
    auto page_object = new PageObject(obj);
    return sciter::value::wrap_asset(page_object);
  }

  sciter::value Doc::loadStandardFont(sciter::astring font_name) {
    auto loaded_font = new Font(m_doc, font_name);
    return sciter::value::wrap_asset(loaded_font);
  }

  sciter::value Doc::loadFont(const sciter::value& buffer, int font_type, bool cid) {
    auto loaded_font = new Font(m_doc, buffer, font_type, cid);
    return sciter::value::wrap_asset(loaded_font);
  }

  sciter::value Doc::createTextObj(sciter::value font, float font_size) {
    FPDF_FONT font_object = font.get_asset<Font>()->unwrap();
    ScopedFPDFPageObject obj(FPDFPageObj_CreateTextObj(m_doc.get(), font_object , font_size));
    auto page_object = new PageObject(obj);
    return sciter::value::wrap_asset(page_object);
  }

  void Doc::close() {
    m_doc.reset();
  }

}