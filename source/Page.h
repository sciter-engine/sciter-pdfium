#ifndef __page_h__
#define __page_h__

#include "sciter-x.h"
#include "sciter-x-graphics.hpp"
#include "fpdf_scopers.h"
#include "fpdf_flatten.h"
#include "fpdf_transformpage.h"
#include "Doc.h"

namespace PDFium {

  sciter::value getError(unsigned long code);

  struct Page : Doc {

    ScopedFPDFPage m_page = {};

    int PAGE_FLATTEN_FAIL = FLATTEN_FAIL;
    int PAGE_FLATTEN_SUCCESS = FLATTEN_SUCCESS;
    int PAGE_FLATTEN_NOTHINGTODO = FLATTEN_NOTHINGTODO;

    int PAGE_FLAT_NORMALDISPLAY = FLAT_NORMALDISPLAY;
    int PAGE_FLAT_PRINT = FLAT_PRINT;

    Page(){}
    
    Page(ScopedFPDFDocument& doc, int index) {
      m_page = ScopedFPDFPage(FPDF_LoadPage(doc.get(), index));
      if(m_page == NULL) {
        throw getError(FPDF_ERR_PAGE);
      }
    }

    Page(ScopedFPDFDocument& doc, int index,double width, double height) {
      m_page = ScopedFPDFPage(FPDFPage_New(doc.get(), index, width, height));
      if(m_page.get() == NULL) {
        throw getError(FPDF_ERR_UNKNOWN);
      }
    }

    FPDF_PAGE unwrap() {
      return m_page.get();
    }

    virtual ~Page(){
      m_page.reset();
    };
    
    double getWidth();
    double getHeight();
    bool hasTransparency();
    sciter::value render(double scale = 1.0);
    int getRotation();
    void setRotation(int direction);
    void insertObject(sciter::value page_object);
    bool removeObject(sciter::value page_object);
    int countObjects();
    sciter::value getObject(int index);
    void generateContent();
    sciter::value textLoadPage();
    sciter::value toDevice(double scale, double x, double y);
    sciter::value toPage(double scale, double x, double y);

    int flatten(int flag);

    void setMediaBox(float left, float bottom, float right, float top);
    void setCropBox(float left, float bottom, float right, float top);
    void setBleedBox(float left, float bottom, float right, float top);
    void setTrimBox(float left, float bottom, float right, float top);
    void setArtBox(float left, float bottom, float right, float top);

    sciter::value getMediaBox();
    sciter::value getCropBox();
    sciter::value getBleedBox();
    sciter::value getTrimBox();
    sciter::value getArtBox();

    sciter::value createAnnot(FPDF_ANNOTATION_SUBTYPE subtype);
    int getAnnotCount();
    sciter::value getAnnot(int index);
    int getAnnotIndex(sciter::value annot);
    bool removeAnnot(int index);

    sciter::value getLinkAtPoint(double x, double y);
    int getLinkZOrderAtPoint(double x, double y);
    sciter::value linkEnumerate();

    SOM_PASSPORT_BEGIN(Page)
    SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
    SOM_FUNCS(
      SOM_FUNC(getWidth),
      SOM_FUNC(getHeight),
      SOM_FUNC(hasTransparency),
      SOM_FUNC(render),
      SOM_FUNC(getRotation),
      SOM_FUNC(setRotation),
      SOM_FUNC(insertObject),
      SOM_FUNC(removeObject),
      SOM_FUNC(countObjects),
      SOM_FUNC(getObject),
      SOM_FUNC(generateContent),
      SOM_FUNC(textLoadPage),
      SOM_FUNC(toDevice),
      SOM_FUNC(toPage),
      SOM_FUNC(flatten),
      SOM_FUNC(setMediaBox),
      SOM_FUNC(setCropBox),
      SOM_FUNC(setBleedBox),
      SOM_FUNC(setTrimBox),
      SOM_FUNC(setArtBox),
      SOM_FUNC(getMediaBox),
      SOM_FUNC(getCropBox),
      SOM_FUNC(getBleedBox),
      SOM_FUNC(getTrimBox),
      SOM_FUNC(getArtBox),
      SOM_FUNC(createAnnot),
      SOM_FUNC(getAnnotCount),
      SOM_FUNC(getAnnot),
      SOM_FUNC(getAnnotIndex),
      SOM_FUNC(removeAnnot),
      SOM_FUNC(getLinkAtPoint),
      SOM_FUNC(getLinkZOrderAtPoint),
      SOM_FUNC(linkEnumerate),
    )
    SOM_PROPS(
      SOM_RO_PROP(PAGE_FLATTEN_FAIL),
      SOM_RO_PROP(PAGE_FLATTEN_SUCCESS),
      SOM_RO_PROP(PAGE_FLATTEN_NOTHINGTODO),

      SOM_RO_PROP(PAGE_FLAT_NORMALDISPLAY),
      SOM_RO_PROP(PAGE_FLAT_PRINT),
    )
    SOM_PASSPORT_END

  };

}
#endif