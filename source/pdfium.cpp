#include "pdfium.h"

namespace PDFium {

  sciter::value PDF::createNewDocument() {
    try {
      auto doc = new Doc();
      return sciter::value::wrap_asset(doc);
    }
    catch (sciter::value e) {
      return e;
    }
  }

  sciter::value PDF::loadDocument(sciter::astring file_path, sciter::astring password) {
    try {
      auto doc = new Doc(file_path, password);
      return sciter::value::wrap_asset(doc);
    }
    catch(sciter::value e){
      return e;
    }    
  }

  sciter::value PDF::loadMemDocument(const sciter::value& buffer, sciter::astring password) {
    try {
      auto doc = new Doc(buffer, password);
      return sciter::value::wrap_asset(doc);
    }
    catch (sciter::value e) {
      return e;
    }
  }

}

extern "C" {

  #ifndef WINDOWS
  __attribute__((visibility("default")))
  #endif
  SBOOL SCAPI SciterLibraryInit(ISciterAPI* psapi, SCITER_VALUE* plibobject)
  {
    _SAPI(psapi); // set reference to Sciter API provided by host application including scapp(quark)
    static sciter::om::hasset<PDFium::PDF> pdf = new PDFium::PDF(); // invloked once (C++ static convention)
    *plibobject = sciter::value::wrap_asset(pdf);
    return TRUE;
  }
}