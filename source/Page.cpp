#include "Page.h"
#include "Object.h"
#include "Text.h"
#include "Annot.h"
#include "Link.h"

#include "fpdf_annot.h"
#include "fpdf_doc.h"

namespace PDFium {

  double Page::getWidth() {
    return FPDF_GetPageWidth(m_page.get());
  }

  double Page::getHeight() {
    return FPDF_GetPageHeight(m_page.get());
  }

  bool Page::hasTransparency() {
    return FPDFPage_HasTransparency(m_page.get());
  }

  sciter::value Page::render(double scale) {

    int width = static_cast<int>(getWidth() * scale);
    int height = static_cast<int>(getHeight() * scale);
    int alpha = hasTransparency() ? 1 : 0;
    ScopedFPDFBitmap bitmap(FPDFBitmap_Create(width, height, alpha));  // BGRx

    if (bitmap.get()) {
      FPDF_DWORD fill_color = alpha ? 0x00000000 : 0xFFFFFFFF;
      FPDFBitmap_FillRect(bitmap.get(), 0, 0, width, height, fill_color);
      int rotation = 0;
      int flags = FPDF_ANNOT | FPDF_LCD_TEXT;
      FPDF_RenderPageBitmap(bitmap.get(), m_page.get(), 0, 0, width, height, rotation, flags);
      BYTE* buffer = (BYTE*)FPDFBitmap_GetBuffer(bitmap.get());
      sciter::image img = sciter::image::create(width, height, false, buffer);
      bitmap.reset();
      m_page.reset();
      return img.to_value();
    }

    throw sciter::om::exception("Could not render page.");
  }

  int Page::getRotation() {
    return FPDFPage_GetRotation(m_page.get());
  }

  void Page::setRotation(int rotation) {
    return FPDFPage_SetRotation(m_page.get(), rotation);
  }

  void Page::insertObject(sciter::value object) {
    auto obj = object.get_asset<PageObject>();
    FPDFPage_InsertObject(m_page.get(), obj->unwrap());
  }

  bool Page::removeObject(sciter::value object) {
    auto page_object = object.get_asset<PageObject>();
    bool response = FPDFPage_RemoveObject(m_page.get(), page_object->unwrap());
    
    page_object->destroy();
    page_object->asset_release();
    return response;
  }

  int Page::countObjects() {
    return FPDFPage_CountObjects(m_page.get());
  }

  sciter::value Page::getObject(int index) {
    ScopedFPDFPageObject obj(FPDFPage_GetObject(m_page.get(), index));
    auto page_object = new PageObject(obj);
    return sciter::value::wrap_asset(page_object);
  }

  void Page::generateContent() {
    FPDFPage_GenerateContent(m_page.get());
  }

  sciter::value Page::textLoadPage() {
    FPDF_TEXTPAGE obj = FPDFText_LoadPage(m_page.get());
    auto text_page = new Text(obj);
    return sciter::value::wrap_asset(text_page);
  }

  sciter::value Page::toDevice(double scale, double x, double y) {
    int device_x, device_y;
    bool response = FPDF_PageToDevice(m_page.get(), 
      0, 0, 
      getWidth() * scale, 
      getHeight() * scale, 
      getRotation(), 
      x, y, 
      &device_x, &device_y
    );

    if (!response) {
      return sciter::value();
      //return sciter::value::make_error("Error in function toDevice");
    }

    sciter::value v;
    v["x"] = device_x;
    v["y"] = device_y;
    return v;
  }
  
  sciter::value Page::toPage(double scale, double x, double y) {
    double page_x, page_y;
    bool response = FPDF_DeviceToPage(m_page.get(), 
      0, 0, 
      getWidth() * scale, 
      getHeight() * scale, 
      getRotation(), 
      x, y, 
      &page_x, &page_y
    );

    if (!response) {
      return sciter::value();
      //return sciter::value::make_error("Error in function toPage");
    }

    sciter::value v;
    v["x"] = page_x;
    v["y"] = page_y;
    return v;
  }

  int Page::flatten(int flag){
    return FPDFPage_Flatten(m_page.get(), flag);
  }

  void Page::setMediaBox(float left, float bottom, float right, float top) {
    FPDFPage_SetMediaBox(m_page.get(), left, bottom, right, top);
  }

  void Page::setCropBox(float left, float bottom, float right, float top) {
    FPDFPage_SetCropBox(m_page.get(), left, bottom, right, top);
  }

  void Page::setBleedBox(float left, float bottom, float right, float top) {
    FPDFPage_SetBleedBox(m_page.get(), left, bottom, right, top);
  }

  void Page::setTrimBox(float left, float bottom, float right, float top) {
    FPDFPage_SetTrimBox(m_page.get(), left, bottom, right, top);
  }

  void Page::setArtBox(float left, float bottom, float right, float top) {
    FPDFPage_SetArtBox(m_page.get(), left, bottom, right, top);
  }

  sciter::value Page::getMediaBox() {
    float left, bottom, right, top;
    FPDFPage_GetMediaBox(m_page.get(), &left, &bottom, &right, &top);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("bottom", bottom);
    val.set_item("right", right);
    val.set_item("top", top);
    return val;
  }

  sciter::value Page::getCropBox() {
    float left, bottom, right, top;
    FPDFPage_GetCropBox(m_page.get(), &left, &bottom, &right, &top);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("bottom", bottom);
    val.set_item("right", right);
    val.set_item("top", top);
    return val;
  }

  sciter::value Page::getBleedBox() {
    float left, bottom, right, top;
    FPDFPage_GetBleedBox(m_page.get(), &left, &bottom, &right, &top);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("bottom", bottom);
    val.set_item("right", right);
    val.set_item("top", top);
    return val;
  }

  sciter::value Page::getTrimBox() {
    float left, bottom, right, top;
    FPDFPage_GetTrimBox(m_page.get(), &left, &bottom, &right, &top);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("bottom", bottom);
    val.set_item("right", right);
    val.set_item("top", top);
    return val;
  }

  sciter::value Page::getArtBox() {
    float left, bottom, right, top;
    FPDFPage_GetCropBox(m_page.get(), &left, &bottom, &right, &top);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("bottom", bottom);
    val.set_item("right", right);
    val.set_item("top", top);
    return val;
  }

  sciter::value Page::createAnnot(FPDF_ANNOTATION_SUBTYPE subtype){
    ScopedFPDFAnnotation annot(FPDFPage_CreateAnnot(m_page.get(), subtype));
    auto page_annot = new Annot(annot);
    return sciter::value::wrap_asset(page_annot);
  }

  int Page::getAnnotCount() {
    return FPDFPage_GetAnnotCount(m_page.get());
  }

  sciter::value Page::getAnnot(int index) {
    ScopedFPDFAnnotation annot(FPDFPage_GetAnnot(m_page.get(), index));
    auto page_annot = new Annot(annot);
    return page_annot->wrap();
  }

  int Page::getAnnotIndex(sciter::value annot) {
    auto page_annot = annot.get_asset<Annot>();
    return FPDFPage_GetAnnotIndex(m_page.get(), page_annot->unwrap());
  }

  bool Page::removeAnnot(int index) {
    return FPDFPage_RemoveAnnot(m_page.get(), index);
  }

  sciter::value Page::getLinkAtPoint(double x, double y) {
    FPDF_LINK fs_link = FPDFLink_GetLinkAtPoint(m_page.get(), x, y);
    auto link = new Link(fs_link);
    return sciter::value::wrap_asset(link);
  }

  int Page::getLinkZOrderAtPoint(double x, double y) {
    return FPDFLink_GetLinkZOrderAtPoint(m_page.get(), x, y);
  }

  sciter::value Page::linkEnumerate() {
    int start_pos = 0;
    FPDF_LINK link_annot = nullptr;
    std::vector<sciter::value> links{};
    
    while (FPDFLink_Enumerate(m_page.get(), &start_pos, &link_annot)) {
      links.push_back(sciter::value::wrap_asset(new Link(link_annot)));
    }
    
    return links;
  }

}