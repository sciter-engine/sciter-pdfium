#ifndef __object_h__
#define __object_h__

#include "sciter-x.h"
#include "fpdfview.h"
#include "fpdf_edit.h"
#include "Page.h"

namespace PDFium {

  sciter::value getError(unsigned long code);

  struct PageObject : Page {
    ScopedFPDFPageObject m_page_object = {};

    int OBJECT_UNKNOWN = FPDF_OBJECT_UNKNOWN;
    int OBJECT_BOOLEAN = FPDF_OBJECT_BOOLEAN;
    int OBJECT_NUMBER = FPDF_OBJECT_NUMBER;
    int OBJECT_STRING = FPDF_OBJECT_STRING;
    int OBJECT_NAME = FPDF_OBJECT_NAME;
    int OBJECT_ARRAY = FPDF_OBJECT_ARRAY;
    int OBJECT_DICTIONARY = FPDF_OBJECT_DICTIONARY;
    int OBJECT_STREAM = FPDF_OBJECT_STREAM;
    int OBJECT_NULLOBJ = FPDF_OBJECT_NULLOBJ;
    int OBJECT_REFERENCE = FPDF_OBJECT_REFERENCE;

    // The page object constants.
    int PAGEOBJ_UNKNOWN = FPDF_PAGEOBJ_UNKNOWN;
    int PAGEOBJ_TEXT = FPDF_PAGEOBJ_TEXT;
    int PAGEOBJ_PATH = FPDF_PAGEOBJ_PATH;
    int PAGEOBJ_IMAGE = FPDF_PAGEOBJ_IMAGE;
    int PAGEOBJ_SHADING = FPDF_PAGEOBJ_SHADING;
    int PAGEOBJ_FORM = FPDF_PAGEOBJ_FORM;

    // The path segment constants.
    int SEGMENT_UNKNOWN = FPDF_SEGMENT_UNKNOWN;
    int SEGMENT_LINETO = FPDF_SEGMENT_LINETO;
    int SEGMENT_BEZIERTO = FPDF_SEGMENT_BEZIERTO;
    int SEGMENT_MOVETO = FPDF_SEGMENT_MOVETO;

    int FILLMODE_NONE = FPDF_FILLMODE_NONE;
    int FILLMODE_ALTERNATE = FPDF_FILLMODE_ALTERNATE;
    int FILLMODE_WINDING = FPDF_FILLMODE_WINDING;

    int LINECAP_BUTT = FPDF_LINECAP_BUTT;
    int LINECAP_ROUND = FPDF_LINECAP_ROUND;
    int LINECAP_PROJECTING_SQUARE = FPDF_LINECAP_PROJECTING_SQUARE;

    int LINEJOIN_MITER = FPDF_LINEJOIN_MITER;
    int LINEJOIN_ROUND = FPDF_LINEJOIN_ROUND;
    int LINEJOIN_BEVEL = FPDF_LINEJOIN_BEVEL;

    PageObject() {}
    PageObject(ScopedFPDFPageObject& object) : m_page_object(move(object)) { }

    virtual ~PageObject() {
      destroy();
      asset_release();
    }

    FPDF_PAGEOBJECT unwrap() {
      asset_add_ref();
      return m_page_object.get();
    }

    inline sciter::value wrap() {
      return sciter::value::wrap_asset(this);
    }

    sciter::value setFillColor(UINT R, UINT G, UINT B, UINT A);
    sciter::value getFillColor();
    sciter::value setDrawMode(int fill_mode, bool stroke);
    sciter::value getDrawMode();

    bool hasTransparency();
    int getType();

    sciter::value transform(sciter::value matrix);
    std::vector<float> getMatrix();
    sciter::value setMatrix(sciter::value array);

    sciter::value getBitmap();
    void destroy();

    sciter::value getBounds();
    sciter::value getRotatedBounds();
    sciter::value setBlendMode(sciter::astring blend_mode);

    sciter::value setStrokeColor(UINT R, UINT G, UINT B, UINT A);
    sciter::value getStrokeColor();

    sciter::value setStrokeWidth(float width);
    float getStrokeWidth();

    int getLineJoin();
    sciter::value setLineJoin(int line_join);

    int getLineCap();
    sciter::value setLineCap(int line_cap);

    float getDashPhase();
    sciter::value setDashPhase(float phase);

    sciter::value moveTo(float x, float y);
    sciter::value lineTo(float x, float y);
    sciter::value bezierTo(sciter::value points);
    bool pathClose();

    sciter::value setText(sciter::string text);

    std::vector<float> getDashArray(INT64 count);
    sciter::value setDashArray(sciter::value dash_array, INT64 dash_count, float phase);

    SOM_PASSPORT_BEGIN(PageObject)
      SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(setFillColor),
        SOM_FUNC(getFillColor),
        SOM_FUNC(setDrawMode),
        SOM_FUNC(getDrawMode),
        SOM_FUNC(hasTransparency),
        SOM_FUNC(getType),
        SOM_FUNC(transform),
        SOM_FUNC(getMatrix),
        SOM_FUNC(setMatrix),
        SOM_FUNC(getBitmap),
        SOM_FUNC(destroy),
        SOM_FUNC(getBounds),
        SOM_FUNC(getRotatedBounds),
        SOM_FUNC(setBlendMode),
        SOM_FUNC(setStrokeColor),
        SOM_FUNC(getStrokeColor),
        SOM_FUNC(setStrokeWidth),
        SOM_FUNC(getStrokeWidth),
        SOM_FUNC(getLineJoin),
        SOM_FUNC(setLineJoin),
        SOM_FUNC(getDashPhase),
        SOM_FUNC(setDashPhase),
        SOM_FUNC(moveTo),
        SOM_FUNC(lineTo),
        SOM_FUNC(bezierTo),
        SOM_FUNC(pathClose),
        SOM_FUNC(setText),
        SOM_FUNC(getDashArray),
        SOM_FUNC(setDashArray),
      )
      SOM_PROPS(
        SOM_RO_PROP(OBJECT_UNKNOWN),
        SOM_RO_PROP(OBJECT_BOOLEAN),
        SOM_RO_PROP(OBJECT_NUMBER),
        SOM_RO_PROP(OBJECT_STRING),
        SOM_RO_PROP(OBJECT_NAME),
        SOM_RO_PROP(OBJECT_ARRAY),
        SOM_RO_PROP(OBJECT_DICTIONARY),
        SOM_RO_PROP(OBJECT_STREAM),
        SOM_RO_PROP(OBJECT_NULLOBJ),
        SOM_RO_PROP(OBJECT_REFERENCE),

        // The page object constants.
        SOM_RO_PROP(PAGEOBJ_UNKNOWN),
        SOM_RO_PROP(PAGEOBJ_TEXT),
        SOM_RO_PROP(PAGEOBJ_PATH),
        SOM_RO_PROP(PAGEOBJ_IMAGE),
        SOM_RO_PROP(PAGEOBJ_SHADING),
        SOM_RO_PROP(PAGEOBJ_FORM),

        // The path segment constants.
        SOM_RO_PROP(SEGMENT_UNKNOWN),
        SOM_RO_PROP(SEGMENT_LINETO),
        SOM_RO_PROP(SEGMENT_BEZIERTO),
        SOM_RO_PROP(SEGMENT_MOVETO),

        SOM_RO_PROP(FILLMODE_NONE),
        SOM_RO_PROP(FILLMODE_ALTERNATE),
        SOM_RO_PROP(FILLMODE_WINDING),

        SOM_RO_PROP(LINECAP_BUTT),
        SOM_RO_PROP(LINECAP_ROUND),
        SOM_RO_PROP(LINECAP_PROJECTING_SQUARE),

        SOM_RO_PROP(LINEJOIN_MITER),
        SOM_RO_PROP(LINEJOIN_ROUND),
        SOM_RO_PROP(LINEJOIN_BEVEL),
      )
    SOM_PASSPORT_END
  };

}
#endif