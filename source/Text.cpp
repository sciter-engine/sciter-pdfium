#include "Text.h"
#include "Search.h"
#include "Link.h"

namespace PDFium {

  void Text::closePage() {
    m_text_page.reset();
  }

  int Text::countChars() {
    return FPDFText_CountChars(m_text_page.get());
  }

  UINT Text::getUnicode(int char_index) {
    return FPDFText_GetUnicode(m_text_page.get(), char_index);
  }

  int Text::isGenerated(int char_index) {
    return FPDFText_IsGenerated(m_text_page.get(), char_index);
  }

  int Text::isHyphen(int char_index) {
    return FPDFText_IsHyphen(m_text_page.get(), char_index);
  }

  int Text::hasUnicodeMapError(int char_index) {
    return FPDFText_HasUnicodeMapError(m_text_page.get(), char_index);
  }

  double Text::getFontSize(int char_index) {
    return FPDFText_GetFontSize(m_text_page.get(), char_index);
  }

  sciter::value Text::getFontInfo(int char_index) {
    
    int flags = -1;
    std::vector<char> font_name(255); //255 = max file name length
    std::fill(font_name.begin(), font_name.end(), ' ');
    
    unsigned long font_str_length = FPDFText_GetFontInfo(
      m_text_page.get(), 
      char_index, 
      font_name.data(), 255, 
      &flags
    ); 
    
    font_name.resize(font_str_length);
    sciter::value val;
    val.set_item("font-name", sciter::value::make_string(font_name.data()));
    val.set_item("flags", flags);
    return val;
  }

  int Text::getFontWeight(int char_index) {
    return FPDFText_GetFontWeight(m_text_page.get(), char_index);
  }

  int Text::getTextRenderMode(int char_index) {
    return FPDFText_GetTextRenderMode(m_text_page.get(), char_index);
  }

  sciter::value Text::getFillColor(int char_index) {
    unsigned int R, G, B, A;
    return FPDFText_GetFillColor(m_text_page.get(), char_index, &R, &G, &B, &A);
    sciter::value::color(A << 24 | B << 16 | G << 8 | R);
  }

  sciter::value Text::getStrokeColor(int char_index) {
    unsigned int R, G, B, A;
    return FPDFText_GetStrokeColor(m_text_page.get(), char_index, &R, &G, &B, &A);
    sciter::value::color(A << 24 | B << 16 | G << 8 | R);
  }

  float Text::getCharAngle(int char_index) {
    return FPDFText_GetCharAngle(m_text_page.get(), char_index);
  }

  sciter::value Text::getCharBox(int char_index) {
    double left, right, bottom, top;

    FPDFText_GetCharBox(m_text_page.get(), char_index, &left, &right, &bottom, &top);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("right", right);
    val.set_item("bottom", bottom);
    val.set_item("top", top);
    return val;
  }

  sciter::value Text::getLooseCharBox(int char_index) {
    FS_RECTF rect;

    FPDFText_GetLooseCharBox(m_text_page.get(), char_index, &rect);

    sciter::value val;
    val.set_item("left", rect.left);
    val.set_item("right", rect.right);
    val.set_item("bottom", rect.bottom);
    val.set_item("top", rect.top);
    return val;
  }

  std::vector<float> Text::getMatrix(int char_index) {
    FS_MATRIX matrix;

    FPDFText_GetMatrix(m_text_page.get(), char_index, &matrix);

    std::vector<float> v = {
      matrix.a, matrix.c, matrix.e,
      matrix.b, matrix.d, matrix.f
    };
    return v;
  }

  sciter::value Text::getCharOrigin(int char_index) {
    double x, y;

    FPDFText_GetCharOrigin(m_text_page.get(), char_index, &x,&y);

    sciter::value val;
    val.set_item("x", x);
    val.set_item("y", y);
    return val;
  }

  int Text::getCharIndexAtPos(double x, double y, double xTolerance, double yTolerance) {
    return FPDFText_GetCharIndexAtPos(m_text_page.get(), x, y, xTolerance, yTolerance);
  }

  sciter::value Text::getText(int start_index, int count) {
    std::vector<unsigned short> buffer(count+1);
    std::fill(buffer.begin(), buffer.end(), u'\0'); //todo: find out best char/eof to fill here

    int str_length = FPDFText_GetText(m_text_page.get(), start_index, count, buffer.data());

    buffer.resize(str_length);
    auto text = reinterpret_cast<const WCHAR*>(buffer.data());
    return sciter::value::make_string(text, str_length + 1);
  }
  
  int Text::countRects(int start_index, int count) {
    return FPDFText_CountRects(m_text_page.get(), start_index, count);
  }

  sciter::value Text::getRect(int rect_index) {
    double left, top, right, bottom;

    FPDFText_GetRect(m_text_page.get(), rect_index, &left, &top, &right, &bottom);

    sciter::value val;
    val.set_item("left", left);
    val.set_item("right", right);
    val.set_item("bottom", bottom);
    val.set_item("top", top);
    return val;
  }

  sciter::value Text::getBoundedText(double left, double top, double right, double bottom) {
    int char_count = FPDFText_GetBoundedText(m_text_page.get(), left, top, right, bottom, NULL, 0);
    if (char_count > 0) {
      std::vector<unsigned short> buffer(char_count + 1);
      std::fill(buffer.begin(), buffer.end(), u'\0');

      char_count = FPDFText_GetBoundedText(m_text_page.get(),
        left,
        top,
        right,
        bottom,
        buffer.data(),
        char_count
      );
      buffer.resize(char_count);
      auto text = reinterpret_cast<const WCHAR*>(buffer.data());
      return sciter::value::make_string(text, char_count + 1);
    }
    return "";
  }
  
  sciter::value Text::findStart(sciter::string _find_what, UINT32 flags, int start_index) {
    FPDF_WIDESTRING find_what = reinterpret_cast<FPDF_WIDESTRING>(_find_what.data());

    FPDF_SCHHANDLE v = FPDFText_FindStart(m_text_page.get(),
      find_what,
      flags,
      start_index
    );

    auto search_handle = new Search(v);
    return sciter::value::wrap_asset(search_handle);
  }

  sciter::value Text::loadWebLinks() {
    FPDF_PAGELINK v = FPDFLink_LoadWebLinks(m_text_page.get());
    auto page_link = new PageLink(v);
    return sciter::value::wrap_asset(page_link);
  }

  int Text::getCharIndexFromTextIndex(int text_index) {
    return FPDFText_GetCharIndexFromTextIndex(m_text_page.get(), text_index);
  }

  int Text::getTextIndexFromCharIndex(int char_index) {
    return FPDFText_GetTextIndexFromCharIndex(m_text_page.get(), char_index);
  }

}