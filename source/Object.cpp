﻿#include "Object.h"
#include "fpdf_scopers.h"
#include "sciter-x-graphics.hpp"

namespace PDFium {

  sciter::value PageObject::setFillColor(UINT R, UINT G, UINT B, UINT A) {
    bool response = FPDFPageObj_SetFillColor(m_page_object.get(), R, G, B, A);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setFillColor");
  }

  sciter::value PageObject::getFillColor() {
    unsigned int R, G, B, A;
    FPDFPageObj_GetStrokeColor(m_page_object.get(), &R, &G, &B, &A);

    sciter::value val;
    val.set_item("R", R);
    val.set_item("G", G);
    val.set_item("B", B);
    val.set_item("A", A);
    return val;
  }

  sciter::value PageObject::setDrawMode(int fill_mode, bool stroke) {
    bool response = FPDFPath_SetDrawMode(m_page_object.get(), fill_mode, stroke);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setDrawMode");
  }

  sciter::value PageObject::getDrawMode() {
    int fillmode;
    FPDF_BOOL stroke;
    FPDFPath_GetDrawMode(m_page_object.get(), &fillmode, &stroke);
    
    sciter::value val;
    val.set_item("fillmode", fillmode);
    val.set_item("stroke", stroke);
    return val;
  }

  bool PageObject::hasTransparency() {
    return FPDFPageObj_HasTransparency(m_page_object.get());
  }

  int PageObject::getType() {
    return FPDFPageObj_GetType(m_page_object.get());
  }

  sciter::value PageObject::transform(sciter::value array) {
    double scaleX = array.get_item(0).get<double>();
    double skewX = array.get_item(1).get<double>();
    double skewY = array.get_item(2).get<double>();
    double scaleY = array.get_item(3).get<double>();
    double posX = array.get_item(4).get<double>();
    double posY = array.get_item(5).get<double>();
    FPDFPageObj_Transform(m_page_object.get(), scaleX, skewX, skewY, scaleY, posX, posY);
    return this->wrap();
  }

  std::vector<float> PageObject::getMatrix() {
    FS_MATRIX matrix = {};
    bool response = FPDFPageObj_GetMatrix(m_page_object.get(), &matrix);
    if (response) {
      std::vector<float> v = {
        matrix.a, matrix.c, matrix.e,
        matrix.b, matrix.d, matrix.f
      };
      return v;
    }
    else {
      throw getError(FPDF_ERR_UNKNOWN);
    }
  }

  sciter::value PageObject::setMatrix(sciter::value array) {
    float scaleX = array.get_item(0).get<float>();
    float scaleY = array.get_item(3).get<float>();

    float skewX = array.get_item(1).get<float>();
    float skewY = array.get_item(2).get<float>();

    float posX = array.get_item(4).get<float>();
    float posY = array.get_item(5).get<float>();
    FS_MATRIX matrix = { scaleX, skewX, skewY, 
                         scaleY, posX,  posY };

    bool response = FPDFPageObj_SetMatrix(m_page_object.get(), &matrix);

    if (response)
      return this->wrap();
    return sciter::value::make_error("Error in function setMatrix");
  }

  sciter::value PageObject::getBitmap() {
    ScopedFPDFBitmap bitmap(FPDFImageObj_GetBitmap(m_page_object.get()));
    void* buffer = FPDFBitmap_GetBuffer(bitmap.get());

    UINT width = FPDFBitmap_GetHeight(bitmap.get());
    UINT height = FPDFBitmap_GetWidth(bitmap.get());

    sciter::image img = sciter::image::create(width, height, false, (BYTE*)buffer);
    return img.to_value();
  }

  void PageObject::destroy() {
    m_page_object.reset();
  }

  sciter::value PageObject::getBounds() {
    float left, bottom, right, top;

    FPDFPageObj_GetBounds(m_page_object.get(), &left, &bottom, &right, &top);
    
    sciter::value val;
    val.set_item("left", left);
    val.set_item("bottom", bottom);
    val.set_item("right", right);
    val.set_item("top", bottom);
    return val;
  }

  sciter::value PageObject::getRotatedBounds()
  {
    FS_QUADPOINTSF quad_points;
    FPDFPageObj_GetRotatedBounds(m_page_object.get(), &quad_points);

    sciter::value val;
    val.set_item("x1", quad_points.x1);
    val.set_item("y1", quad_points.y1);
    val.set_item("x2", quad_points.x2);
    val.set_item("y2", quad_points.y2);
    val.set_item("x3", quad_points.x3);
    val.set_item("y3", quad_points.y3);
    val.set_item("x4", quad_points.x4);
    val.set_item("y4", quad_points.y4);
    return val;
  }

  sciter::value PageObject::setBlendMode(sciter::astring blend_mode) {
    FPDFPageObj_SetBlendMode(m_page_object.get(), blend_mode.c_str());
    return this->wrap();
  }

  sciter::value PageObject::getStrokeColor() {
    unsigned int R, G, B, A;
    FPDFPageObj_GetStrokeColor(m_page_object.get(), &R, &G, &B, &A);

    sciter::value val;
    val.set_item("R", R);
    val.set_item("G", G);
    val.set_item("B", B);
    val.set_item("A", A);
    return val;
  }

  sciter::value PageObject::setStrokeColor(UINT R, UINT G, UINT B, UINT A) {
    bool response = FPDFPageObj_SetStrokeColor(m_page_object.get(), R, G, B, A);
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setStrokeColor");
  }

  float PageObject::getStrokeWidth() {
    float width;
    FPDFPageObj_GetStrokeWidth(m_page_object.get(), &width);
    return width;
  }

  sciter::value PageObject::setStrokeWidth(float width) {
    bool response = FPDFPageObj_SetStrokeWidth(m_page_object.get(), width);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function setStrokeWidth");
  }

  int PageObject::getLineJoin() {
    return FPDFPageObj_GetLineJoin(m_page_object.get());
  }

  sciter::value PageObject::setLineJoin(int line_join) {
    bool response = FPDFPageObj_SetLineJoin(m_page_object.get(), line_join);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function setLineJoin");
  }

  int PageObject::getLineCap() {
    return FPDFPageObj_GetLineCap(m_page_object.get());
  }

  sciter::value PageObject::setLineCap(int line_cap) {
    bool response = FPDFPageObj_SetLineCap(m_page_object.get(), line_cap);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function setLineCap");
  }

  float PageObject::getDashPhase() {
    float phase;
    FPDFPageObj_GetDashPhase(m_page_object.get(), &phase);
    return phase;
  }

  sciter::value PageObject::setDashPhase(float phase) {
    bool response = FPDFPageObj_SetDashPhase(m_page_object.get(), phase);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function setDashPhase");
  }

  sciter::value PageObject::moveTo(float x, float y) {
    bool response = FPDFPath_MoveTo(m_page_object.get(), x, y);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function moveTo");
  }

  sciter::value PageObject::lineTo(float x, float y) {
    bool response = FPDFPath_LineTo(m_page_object.get(), x, y);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function lineTo");
  }

  sciter::value PageObject::bezierTo(sciter::value points) {
    float x1, y1, x2, y2, x3, y3;
    x1 = points.get_item("x1").get<float>();
    y1 = points.get_item("y1").get<float>();
    x2 = points.get_item("x2").get<float>();
    y2 = points.get_item("y2").get<float>();
    x3 = points.get_item("x3").get<float>();
    y3 = points.get_item("y3").get<float>();
    
    bool response = FPDFPath_BezierTo(m_page_object.get(), x1, y1, x2, y2, x3, y3);
    
    return (response) ? this->wrap() : sciter::value::make_error("Error in function bezierTo");
  }

  bool PageObject::pathClose() {
    return FPDFPath_Close(m_page_object.get());
  }

  sciter::value PageObject::setText(sciter::string text) {
    FPDF_WIDESTRING str = reinterpret_cast<FPDF_WIDESTRING>(text.data());
    bool response = FPDFText_SetText(m_page_object.get(), str);
    
    return (response) ? this->wrap() : sciter::value::make_error("Error in function setText");
  }

  std::vector<float> PageObject::getDashArray(INT64 dash_count) {
    std::vector<float> x(dash_count, 0);
    bool response = FPDFPageObj_GetDashArray(m_page_object.get(), x.data(), dash_count);
    return (response && !x.empty()) ? x : std::vector<float>(0);
  }

  sciter::value PageObject::setDashArray(sciter::value dash_array, INT64 dash_count, float phase) {
    std::vector<float> arr = dash_array.get<std::vector<float>>();
    bool response = FPDFPageObj_SetDashArray(m_page_object.get(), arr.data(), dash_count, phase);

    return (response) ? this->wrap() : sciter::value::make_error("Error in function setDashArray");
  }

}