#ifndef __util_hpp__
#define __util_hpp__

#include "sciter-x.h"
#include "fpdfview.h"
#include "fpdf_doc.h"

#include "Doc.h"

namespace PDFium {
  
  struct Dest : sciter::om::asset<Dest> {
    FPDF_DEST m_dest;

    int VIEW_UNKNOWN_MODE = PDFDEST_VIEW_UNKNOWN_MODE;
    int VIEW_XYZ = PDFDEST_VIEW_XYZ;
    int VIEW_FIT = PDFDEST_VIEW_FIT;
    int VIEW_FITH = PDFDEST_VIEW_FITH;
    int VIEW_FITV = PDFDEST_VIEW_FITV;
    int VIEW_FITR = PDFDEST_VIEW_FITR;
    int VIEW_FITB = PDFDEST_VIEW_FITB;
    int VIEW_FITBH = PDFDEST_VIEW_FITBH;
    int VIEW_FITBV = PDFDEST_VIEW_FITBV;

    Dest() : m_dest(nullptr) {}
    Dest(FPDF_DEST dest) : m_dest(dest) {}

    int getDestPageIndex(const sciter::value& fpdf_document) {
      auto document = fpdf_document.get_asset<Doc>();
      int index = FPDFDest_GetDestPageIndex(document->m_doc.get(), m_dest);
      return index;
    }
    
    sciter::value getView(){
      unsigned long pNumParams;
      FS_FLOAT pParams;
      auto view_type = (UINT32)FPDFDest_GetView(m_dest, &pNumParams, &pParams);
      //TODO: return proper values pNumParams and pParams
      return view_type;
    }
  
    sciter::value getLocationInPage() {
      FPDF_BOOL hasXVal = false, hasYVal = false, hasZoomVal = false;
      FS_FLOAT x = -1, y = -1, zoom = -1;
      
      bool response = FPDFDest_GetLocationInPage(m_dest, 
        &hasXVal, &hasYVal, &hasZoomVal,
        &x, &y, &zoom
      );

      if(!response)
        return sciter::value::make_error("Error in function getLocationInPage");

      sciter::value val;
      val.set_item("hasXVal", hasXVal);
      val.set_item("hasYVal", hasYVal);
      val.set_item("hasZoomVal", hasZoomVal);
      val.set_item("x", x);
      val.set_item("y", y);
      val.set_item("zoom", zoom);
      return val;
    }

    SOM_PASSPORT_BEGIN(Dest)
      SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(getDestPageIndex),
        SOM_FUNC(getView),
        SOM_FUNC(getLocationInPage),
      )
      SOM_PROPS(
        SOM_PROP(VIEW_UNKNOWN_MODE),
        SOM_PROP(VIEW_XYZ),
        SOM_PROP(VIEW_FIT),
        SOM_PROP(VIEW_FITH),
        SOM_PROP(VIEW_FITV),
        SOM_PROP(VIEW_FITR),
        SOM_PROP(VIEW_FITB),
        SOM_PROP(VIEW_FITBH),
        SOM_PROP(VIEW_FITBV),
      )
    SOM_PASSPORT_END
  };
  
  struct Action : sciter::om::asset<Action> {
    FPDF_ACTION m_action{};

    int ACTION_UNSUPPORTED = PDFACTION_UNSUPPORTED;
    int ACTION_GOTO = PDFACTION_GOTO;
    int ACTION_REMOTEGOTO = PDFACTION_REMOTEGOTO;
    int ACTION_URI = PDFACTION_URI;
    int ACTION_LAUNCH = PDFACTION_LAUNCH;

    Action() {}
    Action(FPDF_ACTION action) : m_action(action) {}

    UINT32 getType() {
      return FPDFAction_GetType(m_action);
    }

    sciter::value getFilePath() {
      unsigned long size = FPDFAction_GetFilePath(m_action, NULL, 0);
      std::vector<void*> buffer(size);
      size = FPDFAction_GetFilePath(m_action, buffer.data(), buffer.size());
      return buffer;
    }


    SOM_PASSPORT_BEGIN(Action)
      SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(getType),
      )
      SOM_PROPS(
        SOM_PROP(ACTION_UNSUPPORTED),
        SOM_PROP(ACTION_GOTO),
        SOM_PROP(ACTION_REMOTEGOTO),
        SOM_PROP(ACTION_URI),
        SOM_PROP(ACTION_LAUNCH),
      )
    SOM_PASSPORT_END
  };
}

#endif
