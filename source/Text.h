#ifndef __text_h__
#define __text_h__

#include "Page.h"
#include "fpdf_text.h"
#include "fpdf_searchex.h"

namespace PDFium {

  struct Text : Page {

    ScopedFPDFTextPage m_text_page = {};

    int TEXTRENDERMODE_FILL = FPDF_TEXTRENDERMODE_FILL;
    int TEXTRENDERMODE_STROKE = FPDF_TEXTRENDERMODE_STROKE;
    int TEXTRENDERMODE_FILL_STROKE = FPDF_TEXTRENDERMODE_FILL_STROKE;
    int TEXTRENDERMODE_INVISIBLE = FPDF_TEXTRENDERMODE_INVISIBLE;
    int TEXTRENDERMODE_FILL_CLIP = FPDF_TEXTRENDERMODE_FILL_CLIP;
    int TEXTRENDERMODE_STROKE_CLIP = FPDF_TEXTRENDERMODE_STROKE_CLIP;
    int TEXTRENDERMODE_FILL_STROKE_CLIP = FPDF_TEXTRENDERMODE_FILL_STROKE_CLIP;
    int TEXTRENDERMODE_CLIP = FPDF_TEXTRENDERMODE_CLIP;

    Text(){}
    Text(FPDF_TEXTPAGE& text) : m_text_page(text) {}
    virtual ~Text() {
      closePage();
    }

    void closePage();
    int countChars();
    UINT getUnicode(int char_index);
    int isGenerated(int char_index);
    int isHyphen(int char_index);
    int hasUnicodeMapError(int char_index);
    double getFontSize(int char_index);
    sciter::value getFontInfo(int char_index);
    int getFontWeight(int char_index);
    int getTextRenderMode(int char_index);
    sciter::value getFillColor(int char_index);
    sciter::value getStrokeColor(int char_index);
    float getCharAngle(int char_index);
    sciter::value getCharBox(int char_index);
    sciter::value getLooseCharBox(int char_index);
    std::vector<float> getMatrix(int char_index);
    sciter::value getCharOrigin(int char_index);
    int getCharIndexAtPos(double x, double y, double xTolerance, double yTolerance);
    sciter::value getText(int start_index, int count);
    int countRects(int start_index, int count);
    sciter::value getRect(int rect_index);
    sciter::value getBoundedText(double left, double top, double right, double bottom);
    sciter::value findStart(sciter::string find_what, UINT32 flags, int start_index);
    sciter::value loadWebLinks();
    int getCharIndexFromTextIndex(int text_index);
    int getTextIndexFromCharIndex(int char_index);

    SOM_PASSPORT_BEGIN(Text)
      SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(closePage),
        SOM_FUNC(countChars),
        SOM_FUNC(isGenerated),
        SOM_FUNC(isHyphen),
        SOM_FUNC(hasUnicodeMapError),
        SOM_FUNC(getFontSize),
        SOM_FUNC(getFontInfo),
        SOM_FUNC(getFontWeight),
        SOM_FUNC(getTextRenderMode),
        SOM_FUNC(getFillColor),
        SOM_FUNC(getStrokeColor),
        SOM_FUNC(getCharAngle),
        SOM_FUNC(getCharBox),
        SOM_FUNC(getLooseCharBox),
        SOM_FUNC(getMatrix),
        SOM_FUNC(getCharOrigin),
        SOM_FUNC(getCharIndexAtPos),
        SOM_FUNC(getText),
        SOM_FUNC(countRects),
        SOM_FUNC(getRect),
        SOM_FUNC(getBoundedText),
        SOM_FUNC(findStart),
        SOM_FUNC(loadWebLinks),
      )
      SOM_PROPS(
        SOM_RO_PROP(TEXTRENDERMODE_FILL),
        SOM_RO_PROP(TEXTRENDERMODE_STROKE),
        SOM_RO_PROP(TEXTRENDERMODE_FILL_STROKE),
        SOM_RO_PROP(TEXTRENDERMODE_INVISIBLE),
        SOM_RO_PROP(TEXTRENDERMODE_FILL_CLIP),
        SOM_RO_PROP(TEXTRENDERMODE_STROKE_CLIP),
        SOM_RO_PROP(TEXTRENDERMODE_FILL_STROKE_CLIP),
        SOM_RO_PROP(TEXTRENDERMODE_CLIP),
      )
      SOM_PASSPORT_END
  };

}

#endif
