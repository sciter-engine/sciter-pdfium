#ifndef __annot_h__
#define __annot_h__

#include "sciter-x.h"
#include "fpdfview.h"
#include "fpdf_annot.h"
#include "Page.h"

namespace PDFium {
  sciter::value getError(unsigned long code);
  
  struct Link;

  struct Annot : Page {
    int ANNOT_UNKNOWN = FPDF_ANNOT_UNKNOWN;
    int ANNOT_TEXT = FPDF_ANNOT_TEXT;
    int ANNOT_LINK = FPDF_ANNOT_LINK;
    int ANNOT_FREETEXT = FPDF_ANNOT_FREETEXT;
    int ANNOT_LINE = FPDF_ANNOT_LINE;
    int ANNOT_SQUARE = FPDF_ANNOT_SQUARE;
    int ANNOT_CIRCLE = FPDF_ANNOT_CIRCLE;
    int ANNOT_POLYGON = FPDF_ANNOT_POLYGON;
    int ANNOT_POLYLINE = FPDF_ANNOT_POLYLINE;
    int ANNOT_HIGHLIGHT = FPDF_ANNOT_HIGHLIGHT;
    int ANNOT_UNDERLINE = FPDF_ANNOT_UNDERLINE;
    int ANNOT_SQUIGGLY = FPDF_ANNOT_SQUIGGLY;
    int ANNOT_STRIKEOUT = FPDF_ANNOT_STRIKEOUT;
    int ANNOT_STAMP = FPDF_ANNOT_STAMP;
    int ANNOT_CARET = FPDF_ANNOT_CARET;
    int ANNOT_INK = FPDF_ANNOT_INK;
    int ANNOT_POPUP = FPDF_ANNOT_POPUP;
    int ANNOT_FILEATTACHMENT = FPDF_ANNOT_FILEATTACHMENT;
    int ANNOT_SOUND = FPDF_ANNOT_SOUND;
    int ANNOT_MOVIE = FPDF_ANNOT_MOVIE;
    int ANNOT_WIDGET = FPDF_ANNOT_WIDGET;
    int ANNOT_SCREEN = FPDF_ANNOT_SCREEN;
    int ANNOT_PRINTERMARK = FPDF_ANNOT_PRINTERMARK;
    int ANNOT_TRAPNET = FPDF_ANNOT_TRAPNET;
    int ANNOT_WATERMARK = FPDF_ANNOT_WATERMARK;
    int ANNOT_THREED = FPDF_ANNOT_THREED;
    int ANNOT_RICHMEDIA = FPDF_ANNOT_RICHMEDIA;
    int ANNOT_XFAWIDGET = FPDF_ANNOT_XFAWIDGET;
    int ANNOT_REDACT = FPDF_ANNOT_REDACT;

    int COLORTYPE_Color = FPDFANNOT_COLORTYPE_Color;
    int COLORTYPE_InteriorColor = FPDFANNOT_COLORTYPE_InteriorColor;

    ScopedFPDFAnnotation m_annot = {};

    Annot() {}
    //Annot(FPDF_ANNOTATION& annot) : m_annot(annot) {}
    Annot(ScopedFPDFAnnotation& annot) : m_annot(move(annot)) {}
    
    virtual ~Annot() {
      closeAnnot();
      //asset_release();
    }

    FPDF_ANNOTATION unwrap() {
      //asset_add_ref();
      return m_annot.get();
    }

    inline sciter::value wrap() {
      return sciter::value::wrap_asset(this);
    }

    void closeAnnot();
    int getSubtype();
    bool isObjectSupportedSubtype(int subtype);
    sciter::value updateObject(sciter::value object);
    int addInkStroke(sciter::value points);
    bool removeInkList();
    sciter::value appendObject(sciter::value page_object);
    int getObjectCount();
    sciter::value getObject(int index);
    bool removeObject(int index);
    sciter::value setColor(INT type, UINT R, UINT G, UINT B, UINT A);
    sciter::value getColor(INT type);
    bool hasAttachmentPoints();
    sciter::value setAttachmentPoints(unsigned quad_index, sciter::value quad_points);
    sciter::value appendAttachmentPoints(sciter::value quad_points);
    sciter::value getAttachmentPoints(unsigned quad_index);
    sciter::value setRect(sciter::value rect);
    sciter::value getRect();
    sciter::value getVertices();
    unsigned getInkListCount();
    sciter::value getInkListPath(unsigned path_index);
    sciter::value getLine();
    sciter::value setBorder(float horizontal_radius, float vertical_radius, float border_width);
    sciter::value getBorder();
    bool hasKey(sciter::string key);
    int getValueType(sciter::string key);
    sciter::value setStringValue(sciter::string key, sciter::string value);
    sciter::value getStringValue(sciter::string key);
    sciter::value getNumberValue(sciter::string key);
    sciter::value setAP(int appearanceMode, sciter::string value);
    sciter::value getAP(int appearanceMode);
    sciter::value getLinkedAnnot(sciter::string key);
    int getFlags();
    sciter::value setFlags(int flags);
    sciter::value getLink();

    SOM_PASSPORT_BEGIN(Annot)
      SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
      SOM_FUNCS(
        SOM_FUNC(closeAnnot),
        SOM_FUNC(getSubtype),
        SOM_FUNC(isObjectSupportedSubtype),
        SOM_FUNC(updateObject),
        SOM_FUNC(addInkStroke),
        SOM_FUNC(removeInkList),
        SOM_FUNC(appendObject),
        SOM_FUNC(getObjectCount),
        SOM_FUNC(getObject),
        SOM_FUNC(removeObject),
        SOM_FUNC(setColor),
        SOM_FUNC(getColor),
        SOM_FUNC(hasAttachmentPoints),
        SOM_FUNC(setAttachmentPoints),
        SOM_FUNC(appendAttachmentPoints),
        SOM_FUNC(getAttachmentPoints),
        SOM_FUNC(setRect),
        SOM_FUNC(getRect),
        SOM_FUNC(getVertices),
        SOM_FUNC(getInkListCount),
        SOM_FUNC(getInkListPath),
        SOM_FUNC(getLine),
        SOM_FUNC(setBorder),
        SOM_FUNC(getBorder),
        SOM_FUNC(hasKey),
        SOM_FUNC(getValueType),
        SOM_FUNC(setStringValue),
        SOM_FUNC(getStringValue),
        SOM_FUNC(getNumberValue),
        SOM_FUNC(setAP),
        SOM_FUNC(getAP),
        SOM_FUNC(getLinkedAnnot),
        SOM_FUNC(getFlags),
        SOM_FUNC(setFlags),
        SOM_FUNC(getLink),
        )
      SOM_PROPS(
        SOM_RO_PROP(ANNOT_UNKNOWN),
        SOM_RO_PROP(ANNOT_TEXT),
        SOM_RO_PROP(ANNOT_LINK),
        SOM_RO_PROP(ANNOT_FREETEXT),
        SOM_RO_PROP(ANNOT_LINE),
        SOM_RO_PROP(ANNOT_SQUARE),
        SOM_RO_PROP(ANNOT_CIRCLE),
        SOM_RO_PROP(ANNOT_POLYGON),
        SOM_RO_PROP(ANNOT_POLYLINE),
        SOM_RO_PROP(ANNOT_HIGHLIGHT),
        SOM_RO_PROP(ANNOT_UNDERLINE),
        SOM_RO_PROP(ANNOT_SQUIGGLY),
        SOM_RO_PROP(ANNOT_STRIKEOUT),
        SOM_RO_PROP(ANNOT_STAMP),
        SOM_RO_PROP(ANNOT_CARET),
        SOM_RO_PROP(ANNOT_INK),
        SOM_RO_PROP(ANNOT_POPUP),
        SOM_RO_PROP(ANNOT_FILEATTACHMENT),
        SOM_RO_PROP(ANNOT_SOUND),
        SOM_RO_PROP(ANNOT_MOVIE),
        SOM_RO_PROP(ANNOT_WIDGET),
        SOM_RO_PROP(ANNOT_SCREEN),
        SOM_RO_PROP(ANNOT_PRINTERMARK),
        SOM_RO_PROP(ANNOT_TRAPNET),
        SOM_RO_PROP(ANNOT_WATERMARK),
        SOM_RO_PROP(ANNOT_THREED),
        SOM_RO_PROP(ANNOT_RICHMEDIA),
        SOM_RO_PROP(ANNOT_XFAWIDGET),
        SOM_RO_PROP(ANNOT_REDACT),

        SOM_RO_PROP(COLORTYPE_Color),
        SOM_RO_PROP(COLORTYPE_InteriorColor),
      )
    SOM_PASSPORT_END

  };
  
}

#endif