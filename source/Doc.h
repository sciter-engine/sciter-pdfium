#ifndef __doc_h__
#define __doc_h__

#include "sciter-x.h"
#include "sciter-x-graphics.hpp"
#include "fpdf_scopers.h"
#include "fpdf_save.h"
#include <fstream>

namespace PDFium {

  sciter::value getError(unsigned long code);

  struct Doc : FPDF_FILEWRITE, public sciter::om::asset<Doc> {
    ScopedFPDFDocument m_doc = {};
    std::vector<sciter::byte> cache = { 0 };
    std::ofstream filestream;

    int NEW = 0;
    int INCREMENTAL = FPDF_INCREMENTAL;
    int NO_INCREMENTAL = FPDF_NO_INCREMENTAL;
    int REMOVE_SECURITY = FPDF_REMOVE_SECURITY;

    int ANNOT = FPDF_ANNOT;
    int LCD_TEXT = FPDF_LCD_TEXT;
    int NO_NATIVETEXT = FPDF_NO_NATIVETEXT;
    int GRAYSCALE = FPDF_GRAYSCALE;
    int DEBUG_INFO = FPDF_DEBUG_INFO;
    int NO_CATCH = FPDF_NO_CATCH;
    int RENDER_LIMITEDIMAGECACHE = FPDF_RENDER_LIMITEDIMAGECACHE;
    int RENDER_FORCEHALFTONE = FPDF_RENDER_FORCEHALFTONE;
    int PRINTING = FPDF_PRINTING;
    int RENDER_NO_SMOOTHTEXT = FPDF_RENDER_NO_SMOOTHTEXT;
    int RENDER_NO_SMOOTHIMAGE = FPDF_RENDER_NO_SMOOTHIMAGE;
    int RENDER_NO_SMOOTHPATH = FPDF_RENDER_NO_SMOOTHPATH;
    int REVERSE_BYTE_ORDER = FPDF_REVERSE_BYTE_ORDER;
    int CONVERT_FILL_TO_STROKE = FPDF_CONVERT_FILL_TO_STROKE;

    Doc() {
      m_doc = ScopedFPDFDocument(FPDF_CreateNewDocument());
      if (!m_doc.get()) {
        throw getError(FPDF_ERR_UNKNOWN);
      }
    }

    Doc(sciter::astring file_path, sciter::astring password) {
      m_doc = ScopedFPDFDocument(FPDF_LoadDocument(file_path.c_str(), password.c_str()));
      if(!m_doc.get()) {
        unsigned long code = FPDF_GetLastError();
        throw getError(code);
      }
    }

    Doc(const sciter::value& buffer, sciter::astring password) {
      cache = buffer.get<std::vector<sciter::byte>>();
      m_doc = ScopedFPDFDocument(FPDF_LoadMemDocument64(&cache[0], int(cache.size()), password.c_str()));
      if(!m_doc.get()) {
        unsigned long code = FPDF_GetLastError();
        throw getError(code);
      }
    }
    virtual ~Doc(){
      close();
    };



    static int writeBlockCallback(FPDF_FILEWRITE* fileWrite, const void* data, unsigned long size) {
      Doc* doc = static_cast<Doc*>(fileWrite);
      if (doc->filestream.is_open()) {
        doc->filestream.write(static_cast<const char*>(data), size);
      }
      return 1;
    }

    int getFileVersion();
    int getPageCount();
    sciter::value renderPage(int page_number, double scale, int flags);
    sciter::value loadPage(int page_number);
    sciter::value addPage(int page_number, double width, double height);
    void deletePage(int page_number);
    sciter::value createNewRect(float x, float y, float w, float h);
    sciter::value newImageObject();
    bool loadJpegFile(const sciter::value& pages, int count, sciter::value image_object, const sciter::value& buffer);
    bool setBitmap(const sciter::value& pages, int count, sciter::value image_object, const sciter::value& buffer);
    void saveAsCopy(sciter::astring file_path);
    sciter::value createNewPath(float x, float y);
    sciter::value newTextObject(sciter::astring font_name, float font_size);
    sciter::value loadStandardFont(sciter::astring font);
    sciter::value loadFont(const sciter::value& buffer,int font_type,bool cid);
    sciter::value createTextObj(sciter::value font, float font_size);
    void close();

    SOM_PASSPORT_BEGIN(Doc)
    SOM_PASSPORT_FLAGS(SOM_SEALED_OBJECT)
    SOM_FUNCS(
      SOM_FUNC(getFileVersion),
      SOM_FUNC(getPageCount),
      SOM_FUNC(renderPage),
      SOM_FUNC(loadPage),
      SOM_FUNC(addPage),
      SOM_FUNC(deletePage),
      SOM_FUNC(createNewRect),
      SOM_FUNC(newImageObject),
      SOM_FUNC(loadJpegFile),
      SOM_FUNC(setBitmap),
      SOM_FUNC(saveAsCopy),
      SOM_FUNC(createNewPath),
      SOM_FUNC(newTextObject),
      SOM_FUNC(loadStandardFont),
      SOM_FUNC(loadFont),
      SOM_FUNC(createTextObj),
      SOM_FUNC(close),
    )
    SOM_PROPS(
      SOM_RO_PROP(NEW),
      SOM_RO_PROP(INCREMENTAL),
      SOM_RO_PROP(NO_INCREMENTAL),
      SOM_RO_PROP(REMOVE_SECURITY),

      SOM_RO_PROP(ANNOT),
      SOM_RO_PROP(LCD_TEXT),
      SOM_RO_PROP(NO_NATIVETEXT),
      SOM_RO_PROP(GRAYSCALE),
      SOM_RO_PROP(DEBUG_INFO),
      SOM_RO_PROP(NO_CATCH),
      SOM_RO_PROP(RENDER_LIMITEDIMAGECACHE),
      SOM_RO_PROP(RENDER_FORCEHALFTONE),
      SOM_RO_PROP(PRINTING),
      SOM_RO_PROP(RENDER_NO_SMOOTHTEXT),
      SOM_RO_PROP(RENDER_NO_SMOOTHIMAGE),
      SOM_RO_PROP(RENDER_NO_SMOOTHPATH),
      SOM_RO_PROP(REVERSE_BYTE_ORDER),
      SOM_RO_PROP(CONVERT_FILL_TO_STROKE),
    )
    SOM_PASSPORT_END

  };

}

#endif