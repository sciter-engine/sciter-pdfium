#include "Search.h"

namespace PDFium {

  bool Search::findNext() {
    return FPDFText_FindNext(m_handle.get());
  }

  bool Search::findPrev() {
    return FPDFText_FindPrev(m_handle.get());
  }

  int Search::getSchResultIndex() {
    return FPDFText_GetSchResultIndex(m_handle.get());
  }

  int Search::getSchCount() {
    return FPDFText_GetSchCount(m_handle.get());
  }

  void Search::findClose() {
    m_handle.reset();
  }

}